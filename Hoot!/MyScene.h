//
//  MyScene.h
//  Hoot!
//

//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "PlayerNode.h"
#import "EnemyNode.h"

@interface MyScene : SKScene <SKPhysicsContactDelegate>

@property (nonatomic, strong) PlayerNode *player;
@property (nonatomic, strong) EnemyNode *enemy;

@end
