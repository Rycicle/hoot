//
//  EnemyNode.m
//  Hoot!
//
//  Created by Ryan Salton on 19/04/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "EnemyNode.h"

#define ENEMY_SPEED 4

@implementation EnemyNode {
    int enemySpeed;
}

-(void)startMoving
{
    self.enemyDirection = 0;
    enemySpeed = ENEMY_SPEED;
    [self addActionToPlayer];
    
}

-(void)incrementSpeed
{
    if(enemySpeed < 25){
        enemySpeed++;
        [self removeAllActions];
        [self addActionToPlayer];
    }
}

-(void)toggleEnemyDirection
{
    self.enemyDirection = self.enemyDirection == 0 ? 1 : 0;
    [self removeAllActions];
    [self addActionToPlayer];
}

-(void)addActionToPlayer
{
    [self runAction:[SKAction repeatActionForever:[SKAction rotateByAngle:DEGREES_TO_RADIANS(self.enemyDirection == 0 ? enemySpeed : -enemySpeed) duration:0.1]]];
}

@end
