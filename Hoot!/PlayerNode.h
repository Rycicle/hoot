//
//  PlayerNode.h
//  Hoot!
//
//  Created by Ryan Salton on 19/04/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface PlayerNode : SKSpriteNode

@end
