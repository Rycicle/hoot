//
//  main.m
//  Hoot!
//
//  Created by Ryan Salton on 19/04/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
