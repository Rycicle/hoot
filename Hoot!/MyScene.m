//
//  MyScene.m
//  Hoot!
//
//  Created by Ryan Salton on 19/04/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MyScene.h"

static const uint32_t enemyCategory = 0x1 << 1;
static const uint32_t playerCategory = 0x1 << 2;

#define PLAYER_SPEED 15

@implementation MyScene{
    int speedIncrements;
    int rotateLeft;
    int rotateRight;
    NSTimer *speedTimer;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        
        
    }
    return self;
}

-(void)didMoveToView:(SKView *)view
{
    self.view.showsPhysics = YES;
    
    self.physicsWorld.contactDelegate = self;
    self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
    rotateLeft = 0;
    rotateRight = 0;
    
    self.player = [PlayerNode spriteNodeWithColor:[UIColor redColor] size:CGSizeMake(40, 200)];
    self.player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.player.size];
    self.player.physicsBody.dynamic = NO;
    self.player.physicsBody.categoryBitMask = playerCategory;
    self.player.physicsBody.contactTestBitMask = enemyCategory;
    self.player.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    self.player.anchorPoint = CGPointMake(0.5, 0.2);
    [self addChild:self.player];
    
    self.enemy = [EnemyNode spriteNodeWithColor:[UIColor greenColor] size:CGSizeMake(20, 30)];
    self.enemy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.enemy.size];
    self.enemy.physicsBody.dynamic = NO;
    self.enemy.physicsBody.categoryBitMask = enemyCategory;
    self.enemy.physicsBody.contactTestBitMask = playerCategory;
    self.enemy.position = CGPointMake(self.frame.size.width / 2, (self.frame.size.height / 2));
    self.enemy.anchorPoint = CGPointMake(7.2, 0);
    [self.enemy startMoving];
    [self addChild:self.enemy];
    
    [self startIncrementTimerWithInterval:7.0];
    speedIncrements = 0;
}

-(void)startIncrementTimerWithInterval:(int)timeInterval
{
    speedTimer = [NSTimer scheduledTimerWithTimeInterval:timeInterval target:self selector:@selector(incrementEnemySpeed) userInfo:nil repeats:YES];
}

-(void)incrementEnemySpeed
{
    speedIncrements++;
    [self.enemy incrementSpeed];
    if(speedIncrements == 2){
        [speedTimer invalidate];
        [self startIncrementTimerWithInterval:5.0];
    }
    else if(speedIncrements == 4){
        [speedTimer invalidate];
        [self startIncrementTimerWithInterval:2.0];
    }
    else if(speedIncrements == 6){
        [speedTimer invalidate];
        [self startIncrementTimerWithInterval:1.0];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    [self endRotatePlayer];
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if(location.x < (self.frame.size.width / 2)){
            NSLog(@"ROTATE LEFT");
            [self rotatePlayer:0];
            rotateLeft++;
        }
        else{
            NSLog(@"ROTATE RIGHT");
            [self rotatePlayer:1];
            rotateRight++;
        }

    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if(location.x < (self.frame.size.width / 2)){
            rotateLeft--;
        }
        else{
            rotateRight--;
        }
        [self endRotatePlayer];
        
        if(!(rotateLeft == 0 && rotateRight == 0)){
            [self rotatePlayer:(rotateLeft > rotateRight) ? 0 : 1];
        }
    }
    
}

-(void)rotatePlayer:(int)direction
{
    int rotatePlayerBy = direction == 0 ? PLAYER_SPEED : -PLAYER_SPEED;
    SKAction *rotatePlayerAction = [SKAction repeatActionForever:[SKAction rotateByAngle:DEGREES_TO_RADIANS(rotatePlayerBy) duration:0.1]];
    [self.player runAction:rotatePlayerAction];
}

-(void)endRotatePlayer
{
    [self.player removeAllActions];
}

-(int)randNumberBetweenMax:(int)maxInt andMin:(int)minInt
{
    int randNum = minInt + arc4random() % (maxInt - minInt);
    return randNum;
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    int randNumb = [self randNumberBetweenMax:100 andMin:0];
//    NSLog(@"%i",randNumb);
    if(randNumb == 1){
        [self.enemy toggleEnemyDirection];
    }
    
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    NSLog(@"CONTACT");
}


@end
