//
//  Hoot_Tests.m
//  Hoot!Tests
//
//  Created by Ryan Salton on 19/04/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Hoot_Tests : XCTestCase

@end

@implementation Hoot_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
